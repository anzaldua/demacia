<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-12">
					<div id="gtco-logo"><a href="index.php">Demacia</a></div>
				</div>
				<div class="col-xs-10 text-right menu-1 main-nav">
					<ul>
						<li class="active"><a href="#" data-nav-section="home">Inicio</a></li>
						<li><a href="#" data-nav-section="about">Galeria</a></li>
						<li class="btn-cta"><a href="#" data-nav-section="contact"><span>Contactanos</span></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<section id="gtco-hero" class="gtco-cover" style="background-image: url(images/nido.jpg);"  data-section="home"  data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					<div class="display-t">
						<div class="display-tc">
							<h1 class="animate-box" data-animate-effect="fadeIn">El territorio de Demacia</h1>
							<h2>Imagen perteneciente a Dagarracos Demacianos</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>