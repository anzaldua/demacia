<section id="gtco-contact" data-section="contact">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
					<h1>Contactanos</h1>
					<p class="sub">Dignissimos asperiores vitae velit veniam totam fuga molestias accusamus alias autem provident. Odit ab aliquam dolor eius.</p>
					<p class="subtle-text animate-box" data-animate-effect="fadeIn">Contacto	</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-push-6 animate-box">
					<form action="#">
						<div class="form-group">
							<label for="name" class="sr-only">Nombre</label>
							<input type="text" class="form-control" placeholder="Nombre" id="name">
						</div>
						<div class="form-group">
							<label for="email" class="sr-only">Email</label>
							<input type="text" class="form-control" placeholder="Email" id="email">
						</div>
						<div class="form-group">
							<label for="message" class="sr-only">Correo</label>
							<textarea name="message" id="message" class="form-control" cols="30" rows="7" placeholder="Mensaje"></textarea>
						</div>
						<div class="form-group">
							<input type="submit" value="Enviar Mensaje" class="btn btn-primary">
						</div>
					</form>
				</div>
				<div class="col-md-4 col-md-pull-6 animate-box">
					<div class="gtco-contact-info">
						<ul>
							<li class="address">198 West 21th Street, Suite 721 New York NY 10016</li>
							<li class="phone"><a href="tel://1235235598">+ 1235 2355 98</a></li>
							<li class="email"><a href="mailto:info@yoursite.com">info@example.com</a></li>
							<li class="url"><a href="#">http://example.com</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<footer id="gtco-footer" role="contentinfo">
		<div class="container">
			
			<div class="row copyright">
				<div class="col-md-12">
					<p class="pull-left">
						<small class="block">&copy; Alejandro Anzaldua. Todos los derechos reservados</small> 
						<small class="block">Hecho por <a href="#" target="_blank">Madara</a> Otros Trabajos: <a href="https://gitlab.com/anzaldua/" target="_blank">GitLab</a></small>
					</p>
					<p class="pull-right">
						<ul class="gtco-social-icons pull-right">
							<li><a href="#"><i class="icon-twitter"></i></a></li>
							<li><a href="#"><i class="icon-facebook"></i></a></li>
							<li><a href="#"><i class="icon-linkedin"></i></a></li>
							<li><a href="#"><i class="icon-dribbble"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>