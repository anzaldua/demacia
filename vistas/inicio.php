
<div id="page">
			<nav class="gtco-nav" role="navigation">
				<div class="container">
					<div class="row">
						<div class="col-sm-2 col-xs-12">
							<div id="gtco-logo"><a href="index.php">Demacia</a></div>
						</div>
						<div class="col-xs-10 text-right menu-1 main-nav">
							<ul>
								<li class="active"><a href="#" data-nav-section="home">Inicio</a></li>
								<li><a href="#" data-nav-section="champions-areas">Sobre Demacia</a></li>
								<li class="btn-cta"><a href="#" data-nav-section="contact"><span>Contactanos</span></a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</nav>

			<section id="gtco-hero" class="gtco-cover" style="background-image: url(images/demacia.jpg);"  data-section="home"  data-stellar-background-ratio="0.5">
				<div class="overlay"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-md-offset-0 text-center">
							<div class="display-t">
								<div class="display-tc">
									<h1 class="animate-box" data-animate-effect="fadeIn">Conoce la historia de Demacia</h1>
									<p class="gtco-video-link animate-box" data-animate-effect="fadeIn"><a href="https://www.youtube.com/watch?v=zF5Ddo9JdpY" class="popup-vimeo"><i class="icon-controller-play"></i></a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="gtco-champions-areas" data-section="champions-areas">
				<div class="container">
					<div class="row row-pb-md">
						<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
							<h1>Conoce sobre demacia</h1>
							<p class="sub">Demacia sigue siendo uno de los poderes dominantes de Valoran y cuenta con el ejército mejor entrenado y de más alto nivel de Runaterra.</p>
							<p class="subtle-text animate-box" data-animate-effect="fadeIn">Leyendas<span>de Demacia</span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href="campeones"><h3 style="color: #A52A2A;">Campeones de Demacia</h3></a>
									<p>Aqui podras ver una lista extendida de los campeones que pertenecen a <strong class="uppercase">Demacia</strong></p>
								</div>
							</div>

							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href="galeria"><h3 style="color: #A52A2A;">Visiones de Demacia</h3></a>
									<p>En esta seccion podras ver una hermosa galeria de arte por parte del reino de Demacia</p>
								</div>
							</div>

							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href=""><h3 style="color: #A52A2A;">Campeones de Demacia</h3></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed scelerisque sapien. Sed sodales, libero non faucibus rutrum, purus tellus finibus diam, eget ornare tortor leo eget erat. </p>
								</div>
							</div>

						</div>
						<div class="col-md-6">
							
							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href=""><h3 style="color: #A52A2A;">Campeones de Demacia</h3></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed scelerisque sapien. Sed sodales, libero non faucibus rutrum, purus tellus finibus diam, eget ornare tortor leo eget erat. </p>
								</div>
							</div>

							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href=""><h3 style="color: #A52A2A;">Campeones de Demacia</h3></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed scelerisque sapien. Sed sodales, libero non faucibus rutrum, purus tellus finibus diam, eget ornare tortor leo eget erat. </p>
								</div>
							</div>

							<div class="gtco-champions-area-item animate-box">
								<div class="gtco-icon">
									<img src="images/demaciamini.png" alt="">
								</div>
								<div class="gtco-copy">
									<a href=""><h3 style="color: #A52A2A;">Campeones de Demacia</h3></a>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed scelerisque sapien. Sed sodales, libero non faucibus rutrum, purus tellus finibus diam, eget ornare tortor leo eget erat. </p>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>';