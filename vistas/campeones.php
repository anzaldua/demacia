<div class="gtco-loader"></div>
	
	<div id="page">
	<nav class="gtco-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-sm-2 col-xs-12">
					<div id="gtco-logo"><a href="index.php">Demacia</a></div>
				</div>
				<div class="col-xs-10 text-right menu-1 main-nav">
					<ul>
						<li class="active"><a href="#" data-nav-section="home">Inicio</a></li>
						<li><a href="#" data-nav-section="about">Areas de Campeones</a></li>
						<li class="btn-cta"><a href="#" data-nav-section="contact"><span>Contactanos</span></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</nav>

	<section id="gtco-hero" class="gtco-cover" style="background-image: url(images/campeonesDemacia.jpg);"  data-section="home"  data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-offset-0 text-center">
					<div class="display-t">
						<div class="display-tc">
							<h1 class="animate-box" data-animate-effect="fadeIn">Los Campeones de Demacia</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="gtco-about" data-section="about">
				<div class="container">
					<div class="row row-pb-md">
						<div class="col-md-8 col-md-offset-2 heading animate-box" data-animate-effect="fadeIn">
							<h1>Campeones</h1>
							<p class="sub"></p>
							<p class="subtle-text animate-box" data-animate-effect="fadeIn">Sylas</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-md-pull-1 animate-box" data-animate-effect="fadeInLeft">
							<div class="img-shadow">
								<img src="images/sylas.jpg" class="img-responsive" alt="">
							</div>
						</div>
						<div class="col-md-6 animate-box" data-animate-effect="fadeInLeft">
							<h2 class="heading-colored">Sylas, el usurpador</h2>
							<p>Como un mago nacido en una familia demaciana humilde, Sylas de Dregbourne probablemente ya estaba condenado desde el principio. A pesar de su clase social baja, sus padres creían firmemente en los ideales del país. Así que cuando descubrieron que su hijo estaba ""aquejado"" de habilidades mágicas, lo convencieron de entregarse a los cazadores de magos del reino. Al notar la extraña habilidad del muchacho para percibir la magia, los cazadores usaron a Sylas para identificar a otros magos que vivieran entre los ciudadanos. Por primera vez en su vida, sintió que tenía un futuro, una vida de servicio a su país, y realizó esas tareas con devoción. Estaba orgulloso, pero se sentía solo: no se le permitía relacionarse con nadie que no fueran sus entrenadores.</p>
							<p id="lee-mas-sylas" style="color: #A52A2A;">Leer Mas!! <i class="icon-chevron-right"> </i><span class="icon-open-book"></span></p>
						</div>
					</div>
					<br><br><br>
					<div class="row gtco-team-reverse">
						<div class="col-md-6 col-md-push-7 animate-box" data-animate-effect="fadeInLeft">
							<div class="img-shadow">
								<img src="images/garen.jpg" class="img-responsive" alt="">
							</div>
						</div>
						<div class="col-md-6 animate-box col-md-pull-6" data-animate-effect="fadeInLeft">
							<h2 class="heading-colored">Garen, el poder de demacia</h2>
							<p>Nacido en la noble familia Guardia de la Corona, junto con su hermana menor Lux, Garen supo desde temprana edad que se esperaba que él defendiera el trono de Demacia con su vida. Su padre, Pieter, era un oficial militar condecorado, mientras que su tía Tianna era la Capitana de la espada de la Vanguardia Valerosa, una fuerza militar de élite; ambos fueron reconocidos y muy respetados por el rey Jarvan III. Por lo tanto, se daba por hecho que, con el correr del tiempo, Garen le serviría al hijo del rey de la misma manera.</p>
							<p id="lee-mas-garen" style="color: #A52A2A;">Leer Mas!! <i class="icon-chevron-right"></i><span class="icon-open-book"></span></p>
						</div>
					</div>
				</div>
			</section>