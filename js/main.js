;(function () {
	
	'use strict';

	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
			BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
			iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
			Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
			Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
			any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};



	var parallax = function() {
		if ( !isMobile.any()) {
			$(window).stellar();
		}
	};

	var mobileMenuOutsideClick = function() {

		$(document).click(function (e) {
	    var container = $("#gtco-offcanvas, .js-gtco-nav-toggle");
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	    	if ( $('body').hasClass('offcanvas') ) {
    			$('body').removeClass('offcanvas');
    			$('.js-gtco-nav-toggle').removeClass('active');
	    	}
	    }
		});

	};

	

	var header = function() {
		$(window).scroll(function(){
			var st = $(window).scrollTop();
			if (st > 50) {
				$('.gtco-nav').addClass('scrolled');
			} else {
				$('.gtco-nav').removeClass('scrolled');
			}
		});
   
	};

	var navigation = function() {

		$('body').on('click', '#gtco-offcanvas ul a:not([class="external"]), .main-nav a:not([class="external"])', function(event){
			var section = $(this).data('nav-section');
				if ( $('[data-section="' + section + '"]').length ) {
			    	$('html, body').animate({
			        	scrollTop: $('[data-section="' + section + '"]').offset().top - 55
			    	}, 500, 'easeInOutExpo');
			   }

			   if ($('body').hasClass('offcanvas')) {
			   	$('body').removeClass('offcanvas');
			   	$('.js-gtco-nav-toggle').removeClass('active');
			   }
		   event.preventDefault();
		   return false;
		});

	};


	var offcanvasMenu = function() {

		$('body').prepend('<div id="gtco-offcanvas" />');
		$('body').prepend('<a href="#" class="js-gtco-nav-toggle gtco-nav-toggle"><i></i></a>');
		var clone1 = $('.menu-1 > ul').clone();
		$('#gtco-offcanvas').append(clone1);
		var clone2 = $('.menu-2 > ul').clone();
		$('#gtco-offcanvas').append(clone2);

		$('#gtco-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
		$('#gtco-offcanvas')
			.find('li')
			.removeClass('has-dropdown');

		// Hover dropdown menu on mobile
		$('.offcanvas-has-dropdown').mouseenter(function(){
			var $this = $(this);

			$this
				.addClass('active')
				.find('ul')
				.slideDown(500, 'easeOutExpo');				
		}).mouseleave(function(){

			var $this = $(this);
			$this
				.removeClass('active')
				.find('ul')
				.slideUp(500, 'easeOutExpo');				
		});


		$(window).resize(function(){

			if ( $('body').hasClass('offcanvas') ) {

    			$('body').removeClass('offcanvas');
    			$('.js-gtco-nav-toggle').removeClass('active');
				
	    	}
		});
	};


	// Reflect scrolling in navigation
	var navActive = function(section) {

		var $el = $('.main-nav > ul');
		$el.find('li').removeClass('active');
		$el.each(function(){
			$(this).find('a[data-nav-section="'+section+'"]').closest('li').addClass('active');
		});

	};

	var navigationSection = function() {

		var $section = $('section[data-section]');
		
		$section.waypoint(function(direction) {
		  	
		  	if (direction === 'down') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
	  		offset: '150px'
		});

		$section.waypoint(function(direction) {
		  	if (direction === 'up') {
		    	navActive($(this.element).data('section'));
		  	}
		}, {
		  	offset: function() { return -$(this.element).height() + 155; }
		});

	};

	var burgerMenu = function() {

		$('body').on('click', '.js-gtco-nav-toggle', function(event){
			var $this = $(this);


			if ( $('body').hasClass('offcanvas') ) {
				$('body').removeClass('offcanvas');
			} else {
				$('body').addClass('offcanvas');
			}
			$this.toggleClass('active');
			event.preventDefault();

		});
	};



	var contentWayPoint = function() {
		var i = 0;
		$('.animate-box').waypoint( function( direction ) {

			if( direction === 'down' && !$(this.element).hasClass('animated-fast') ) {
				
				i++;

				$(this.element).addClass('item-animate');
				setTimeout(function(){

					$('body .animate-box.item-animate').each(function(k){
						var el = $(this);
						setTimeout( function () {
							var effect = el.data('animate-effect');
							if ( effect === 'fadeIn') {
								el.addClass('fadeIn animated-fast');
							} else if ( effect === 'fadeInLeft') {
								el.addClass('fadeInLeft animated-fast');
							} else if ( effect === 'fadeInRight') {
								el.addClass('fadeInRight animated-fast');
							} else {
								el.addClass('fadeInUp animated-fast');
							}

							el.removeClass('item-animate');
						},  k * 200, 'easeInOutExpo' );
					});
					
				}, 100);
				
			}

		} , { offset: '85%' } );
	};


	var dropdown = function() {

		$('.has-dropdown').mouseenter(function(){

			var $this = $(this);
			$this
				.find('.dropdown')
				.css('display', 'block')
				.addClass('animated-fast fadeInUpMenu');

		}).mouseleave(function(){
			var $this = $(this);

			$this
				.find('.dropdown')
				.css('display', 'none')
				.removeClass('animated-fast fadeInUpMenu');
		});

	};


	var owlCarousel = function(){
		
		var owl = $('.owl-carousel-carousel');
		owl.owlCarousel({
			items: 3,
			loop: true,
			margin: 20,
			nav: true,
			dots: true,
			smartSpeed: 800,
			navText: [
		      "<i class='ti-arrow-left owl-direction'></i>",
		      "<i class='ti-arrow-right owl-direction'></i>"
	     	],
	     	responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        1000:{
	            items:3
	        }
	    	}
		});


		var owl = $('.owl-carousel-fullwidth');
		owl.owlCarousel({
			items: 1,
			loop: true,
			margin: 20,
			nav: true,
			dots: true,
			smartSpeed: 800,
			autoHeight: true,
			navText: [
		      "<i class='ti-arrow-left owl-direction'></i>",
		      "<i class='ti-arrow-right owl-direction'></i>"
	     	]
		});

	};


	var goToTop = function() {

		$('.js-gotop').on('click', function(event){
			
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500, 'easeInOutExpo');
			
			return false;
		});

		$(window).scroll(function(){

			var $win = $(window);
			if ($win.scrollTop() > 200) {
				$('.js-top').addClass('active');
			} else {
				$('.js-top').removeClass('active');
			}

		});
	
	};


	// Loading page
	var loaderPage = function() {
		$(".gtco-loader").fadeOut("slow");
	};

	


	
	$(function(){
			parallax();
			mobileMenuOutsideClick();
			header();
			navigation();
			offcanvasMenu();
			burgerMenu();
			navigationSection();
			contentWayPoint();
			dropdown();
			goToTop();
			loaderPage();
		});


	}());

	$(document).on("click", "#lee-mas-sylas", function(){

		$("#lee-mas-sylas").addClass("read-more-sylas")

		$("#lee-mas-sylas").html('<p style="color:#666;">A raíz de su trabajo, Sylas comenzó a darse cuenta de que la magia era mucho más frecuente de lo que Demacia se dignaba a admitir. Podía presentir destellos de poder oculto incluso entre los ricos y distinguidos... algunos eran los detractores más abiertamente declarados de los magos. Mientras a los pobres se los castigaba por sus aflicciones, la élite parecía estar por encima de la ley, y esta hipocresía plantó las primeras semillas de duda en la mente de Sylas.'+

		'Esas dudas finalmente germinaron en un letal evento fatídico, cuando él y sus entrenadores se encontraron con una maga viviendo a escondidas en el campo. Luego de descubrir que se trataba de una niña, Sylas sintió pena por ella. Cuando quiso proteger a la niña de los cazadores de magos, accidentalmente le rozó la piel. La magia de la niña recorrió el cuerpo de Sylas, pero en vez de matarlo, salió disparada de sus manos en explosiones salvajes e incontrolables. Era un talento que no sabía que poseía y terminó con la muerte de tres personas, entre ellas su mentor.'+

		'Sabiendo que lo acusarían de asesinato, Sylas se dio a la fuga y rápidamente se ganó la fama de ser uno de los magos más peligrosos de Demacia. En efecto, cuando los cazadores de magos lo encontraron, no tuvieron piedad.'+

		'Aunque aún era muy joven, Sylas fue sentenciado a cadena perpetua.'+

		'Fue languideciéndose en las profundidades más oscuras del recinto de los cazadores de magos, obligado a llevar pesados grilletes de petricita que atenuaban la magia. Privado de su vista arcana, su corazón se volvió tan duro como la piedra que lo contenía, y soñaba con vengarse de todos los que lo habían puesto allí.'+

		'Luego de quince miserables años, una joven voluntaria de los Iluminadores llamada Luxanna comenzó a visitarlo. A pesar de sus grilletes, Sylas la identificó como una maga particularmente poderosa y, con el tiempo, los dos forjaron un lazo secreto e inusual. A cambio del conocimiento de Sylas sobre el control de la magia, Lux le enseñaba sobre el mundo fuera de su celda y le traía cualquier libro que deseara.'+

		'Con el tiempo, a través de una cuidadosa manipulación, Sylas la convenció de entregarle un tomo prohibido: los escritos originales del gran escultor Durand, en los que detallaba su trabajo con la petricita.'+

		'El volumen le reveló a Sylas los secretos de la piedra. La petricita era la base de las defensas de Demacia contra la hechicería, pero Sylas comprendió que no suprimía la magia, sino que la absorbía.'+

		'Y si el poder se encontraba dentro de la petricita, ¿podría Sylas liberarla...?'+

		'Todo lo que necesitaba era una fuente de magia. Una fuente como Lux.'+

		'Pero ella no volvió a visitarlo. Su familia, la inmensamente poderosa Guardia de la Corona, se había enterado del contacto que tenían y estaban furiosos de que Lux hubiera infringido la ley para ayudar a este vil criminal. Sin dar explicaciones, se decidió que Sylas sería colgado.</p>'+

		'')

		})

	$(document).on("click", ".read-more-sylas", function(){

		$("#lee-mas-sylas").removeClass("read-more-sylas")

		$("#lee-mas-sylas").html('Leer Mas!! <i class="icon-chevron-right"></i><span class="icon-open-book"></span>')

		})

	$(document).on("click", "#lee-mas-garen", function(){

		$("#lee-mas-garen").addClass("read-more-garen")

		$("#lee-mas-garen").html('<p style="color:#666;">El reino de Demacia se había levantado de las cenizas de las Guerras Rúnicas, y los siglos subsecuentes estuvieron plagados de conflictos y de luchas. Uno de los tíos de Garen, un caballero guardián del ejército demaciano, solía contarles a los pequeños Garen y Lux historias sobre sus aventuras del otro lado de las murallas del reino para proteger a su gente de los peligros del mundo que estaba más allá.'+

		'Les advirtió que, sin duda, un día algo le pondría fin a ese tiempo de aparente paz; ya fueran magos rebeldes, criaturas del abismo u otros horrores inimaginables por venir.'+

		'Como confirmación de esos temores, fue asesinado por un mago en el cumplimiento de su deber, antes de que Garen cumpliera once años. Garen presenció el dolor que esto trajo a su familia, así como el miedo en los ojos de su hermana menor. En ese momento, supo con certeza que la magia era el principal y el más grande peligro que Demacia enfrentaba, por lo que juró que no la dejaría traspasar sus murallas. El reino podría mantenerse a salvo solo si se seguían sus ideales fundacionales y se desplegaba su sólido orgullo.'+

		'Cuando cumplió doce años, Garen dejó el hogar familiar de Guardia de la Corona en Lago Plateado para unirse al ejército. Como escudero, sus días y sus noches se consumieron en el entrenamiento y el estudio de la guerra, perfeccionando cuerpo y mente para que se convirtieran en un arma tan poderosa y verdadera como el acero demaciano. Fue entonces cuando conoció al joven Jarvan IV, el príncipe al que, como rey, serviría algún día; él se encontraba entre los otros reclutas y se volvieron inseparables.</p>')

		})	

	$(document).on("click", ".read-more-garen", function(){

		$("#lee-mas-garen").removeClass("read-more-garen")

		$("#lee-mas-garen").html('Leer Mas!! <i class="icon-chevron-right"></i><span class="icon-open-book"></span>')

		})

